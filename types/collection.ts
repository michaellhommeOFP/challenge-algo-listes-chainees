export interface Collection {
  isEmpty(): boolean
  length(): number
  clear(): void
}
