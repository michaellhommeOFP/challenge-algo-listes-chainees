import { Collection } from './collection'


export interface Stack<T> extends Collection {
  push(value: T): void
  pop(): T
  get(i: number): T | null
}
