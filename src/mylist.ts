import { Link, MyStack } from './mystack'
import { List, ForEachCallback, MapCallback, ReduceCallback } from '@list'


export class MyList<T> extends MyStack<T> implements List<T> {


  public insert(i: number, value: T): void {
    throw new Error('not yet implemented')
  }


  public remove(i: number): void {
    throw new Error('not yet implemented')
  }


  public forEach(callback: ForEachCallback<T>): void {
    throw new Error('not yet implemented')
  }


  public map(callback: MapCallback<T>): T[] {
    throw new Error('not yet implemented')
  }


  public reduce<A>(acc: A, callback: ReduceCallback<T, A>): A {
    throw new Error('not yet implemented')
  }


}