import { Queue } from '@queue'
import { ForEachCallback, MapCallback, ReduceCallback } from '@list'

import { Link } from './mystack'
import { MyList } from './mylist'


export class MyFirstQueue<T> extends MyList<T> implements Queue<T> {

  public pushEnd(value: T): void {
    throw new Error('not yet implemented')
  }


  public popEnd(): T {
    throw new Error('not yet implemented')
  }


  forEachR(callback: ForEachCallback<T>): void {
    throw new Error('not yet implemented')
  }


  mapR(callback: MapCallback<T>): T[] {
    throw new Error('not yet implemented')
  }


  reduceR<A>(acc: A, callback: ReduceCallback<T, A>): A {
    throw new Error('not yet implemented')
  }

}