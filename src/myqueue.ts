import { Queue } from '@queue'
import { ForEachCallback, MapCallback, ReduceCallback } from '@list'


export interface DLink<T> {
  value: T
  next: DLink<T> | null
  prev: DLink<T> | null
}


export class MyQueue<T> implements Queue<T> {

  protected head: DLink<T> | null
  protected tail: DLink<T> | null


  constructor() {
    this.head = null
    this.tail = null
  }


  public isEmpty(): boolean {
    throw new Error('not yet implemented')
  }


  public push(value: T): void {
    throw new Error('not yet implemented')
  }


  public pop(): T {
    throw new Error('not yet implemented')
  }


  public length(): number {
    throw new Error('not yet implemented')
  }


  public get(i: number): T {
    throw new Error('not yet implemented')
  }


  public clear(): void {
    throw new Error('not yet implemented')
  }


  public insert(i: number, value: T): void {
    throw new Error('not yet implemented')
  }


  public remove(i: number): void {
    throw new Error('not yet implemented')
  }


  public forEach(callback: ForEachCallback<T>): void {
    throw new Error('not yet implemented')
  }


  public map(callback: MapCallback<T>): T[] {
    throw new Error('not yet implemented')
  }


  public reduce<A>(acc: A, callback: ReduceCallback<T, A>): A {
    throw new Error('not yet implemented')
  }


  public pushEnd(value: T): void {
    throw new Error('not yet implemented')
  }


  public popEnd(): T {
    throw new Error('not yet implemented')
  }


  public forEachR(callback: ForEachCallback<T>): void {
    throw new Error('not yet implemented')
  }


  public mapR(callback: MapCallback<T>): T[] {
    throw new Error('not yet implemented')
  }


  public reduceR<A>(acc: A, callback: ReduceCallback<T, A>): A {
    throw new Error('not yet implemented')
  }

}