# Les structures en chaîne double

## Chaînon
Chaque élément de la chaine contient une valeur ainsi qu'un lien vers son suivant (ou `null` s'il n'y en a pas) et également son précédent (ou `null` s'il n'y en a pas)

```ts
export interface DLink<T> {
  value: T
  next: DLink<T> | null
  prev: DLink<T> | null
}
```

## Structure
Représentation des données pour stocker les valeurs 1, 2, 3 et 4

```mermaid
graph LR
    H((head))-->1
    1-->2
    2-->1
    2-->3
    3-->2
    3-->4
    4-->3
```


## Parcours 

Pour parcourir la chaîne, se déplacer d'élément en élément tant qu'il y a un suivant (ou autre condition) :
- en avant, comme pour la [liste simple](./Link.md)
- en arrière, comme pour la liste mais en partant de `tail` et en utilisant `prev`

```mermaid
graph LR
    S((start))
    head[set current to tail]
    process[process current value]
    test{current has prev ?}
    prev[set current to prev]
    E((end))
    
    S --> head
    head --> process
    process --> test
    prev --> process
    test -- no --> E
    test -- yes --> prev
```


## Insertion

Lors d'une insertion à une position donnée, lier le nouvel élément à son suivant, puis lier l'élément courant vers le nouveau

Algorithme :
```mermaid
graph LR
    S((start))
    move[move to position]
    updatenew[link new to next]
    updatenextP[link next.prev to new]
    updatecurrent[link current to new]
    updatenewP[link new.prev to current]
    E((end))

    S --> move
    move --> updatenew
    updatenew --> updatenextP
    updatenextP --> updatecurrent
    updatecurrent --> updatenewP
    updatenewP --> E
```

Structure pour insertion de `4` entre `3` et `5` :
```mermaid
graph LR
    H((head))-->1
    1-->2
    2-->1
    2-->3
    3-->2
    3-->4
    4-->3
    4-->5
    5-->4

    3-->5
    5-->3

    style 4 stroke:#6f6,stroke-width:2px
    linkStyle 5 stroke:#6f6
    linkStyle 6 stroke:#6f6
    linkStyle 7 stroke:#6f6
    linkStyle 8 stroke:#6f6
    linkStyle 9 stroke:#f66
    linkStyle 10 stroke:#f66
```

## Suppression

Pour la suppression à une position donnée, lier l'élément courant à l'élément suivant celui à supprimer

Algorithme :
```mermaid
graph LR
    S((start))
    move[move to position]
    updatecurrent[link current to next.next]
    updatenext[link next.prev to current]
    E((end))

    S --> move
    move --> updatecurrent
    updatecurrent --> updatenext
    updatenext --> E
```

Structure pour suppression de `4` entre `3` et `5` :
```mermaid
graph LR
    H((head))-->1
    1-->2
    2-->1
    2-->3
    3-->2
    3-->4
    4-->3
    4-->5
    5-->4

    3-->5
    5-->3

    style 4 stroke:#f66,stroke-width:2px
    linkStyle 5 stroke:#f66
    linkStyle 6 stroke:#f66
    linkStyle 7 stroke:#f66
    linkStyle 8 stroke:#f66
    linkStyle 9 stroke:#6f6
    linkStyle 10 stroke:#6f6
```
