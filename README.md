[![pipeline status](../../../badges/master/pipeline.svg)](https://gitlab.com/MichaelLhomme/test_stack/-/commits/master)

# Défis algo -- Structures de données chaînées
    
### Challenge
                                                                                                                  
![hero](../-/jobs/artifacts/master/raw/badges/hero.svg?job=tests)    
![linter](../-/jobs/artifacts/master/raw/badges/linter.svg?job=tests)    
![coverage report](../../../badges/master/coverage.svg)    

![stack](../-/jobs/artifacts/master/raw/badges/MyStack.svg?job=tests) ![list](../-/jobs/artifacts/master/raw/badges/MyList.svg?job=tests) ![firstqueue](../-/jobs/artifacts/master/raw/badges/MyFirstQueue.svg?job=tests) ![queue](../-/jobs/artifacts/master/raw/badges/MyQueue.svg?job=tests)    

## Objectifs

Implémenter des conteneurs de données avec chaînage en utilisant Typescript.

Pour chaque conteneur une série de tests est fournie, ainsi que les interfaces et le squelette de l'implémentation.

> :bulb: L'ordre des tests permet de guider l'implémentation

## Contexte

Dans les languages compilés la structure classique de tableau impose de sérieuses limites au niveau mémoire : difficilement redimensionnable et problème d'allocation sur les grandes tailles. Ils sont donc principalement utilisés lorsque la taille est fixe et connue à l'avance.

Des conteneurs spécifiques sont utilisés pour gérer tous les autres cas :
- Liste pour stocker des collections de taille variable : idéal pour parcours, accès par index coûteux
- Pile **LIFO** *(Last In First Out)*  : pour traiter les données arrivées en dernier en priorité
- File **FIFO** *(First In First Out)* : pour traiter les données arrivées en premier en priorité
- ArrayList : une liste de tableau pour trouver un compris entre problèmes mémoires et vitesse d'accès par index

> :bulb: Dans les languages interprétés les tableaux sont directement implémentés en utilisant une structure de ce type pour ne pas imposer ces restrictions

## Utilisation

- Faire un **fork** de ce dépôt
- Forcer un job CI pour mettre à jour les badges : **CI/CD** > **Pipelines** > **Run pipeline**
- Clôner localement
- Installer les dépendances avec `yarn install` ou `npm install`
- Commencer l'implémentation en suivant les tests
- Les badges sont mis à jour via intégration continue lorsque des changements sont poussés sur Gitlab

## Implémentation

Toutes les implémentations suivent le contrat de base de l'interface `Collection` définie dans le fichier **types/collection.ts**
    
### MyStack
![stack](../-/jobs/artifacts/master/raw/badges/MyStack.svg?job=tests)

Simple pile *LIFO*

- Le contrat est défini par l'interface `Stack` dans **types/stack.ts**
- Le fichier **src/mystack.ts** contient le squelette de la classe `MyStack` ainsi que `Link`, une structure de [chaînon](./Link.md) pour représenter les données

Lancer les tests :

    // Valider l'implémentation
    yarn test 01_mystack.test.js

    // Travailler un test en particulier
    yarn test 01_mystack.test.js -t 'cannot pop when empty'

    
### MyList
![list](../-/jobs/artifacts/master/raw/badges/MyList.svg?job=tests)

Une liste chaînée avec parcours et modification

- Le contrat est défini par l'interface `List` dans **types/list.ts** qui ajoute des fonctionnalités de parcours et de modification à l'interface `Stack`.
- Le fichier **src/mylist.ts** contient le squelette de la classe `MyList`, qui hérite de `MyStack` pour les fonctionnalités de base.
- Les tests de **tests/stack.js** doivent continuer de passer
- Suivre les tests unitaires définis dans **tests/list.js** pour les nouvelles méthodes.

Lancer les tests :

    // Valider l'implémentation
    yarn test 02_mylist.test.js

    // Travailler un test en particulier
    yarn test 02_mylist.test.js -t 'cannot pop when empty'    

    
### MyFirstQueue
![firstqueue](../-/jobs/artifacts/master/raw/badges/MyFirstQueue.svg?job=tests)

Première implémentation d'une queue *FIFO* en réutilisant la liste chaînée

- Le contrat est défini par l'interface `Queue` dans **types/queue.ts**, qui ajoute des fonctionnalités d'ajout en fin de chaîne et de parcours à l'envers.
- Le fichier **src/myfirstqueue.ts** contient le squelette de la classe `MyFirstQueue` qui hérite de `MyList` pour les fonctionnalités de base.
- Les tests de **tests/stack.js** et **tests/list.js** doivent continuer de passer
- Suivre les tests unitaires définis dans **tests/queue.js** pour les nouvelles méthodes.    

Lancer les tests :

    // Valider l'implémentation
    yarn test 03_myfirstqueue.test.js

    // Travailler un test en particulier
    yarn test 03_myfirstqueue.test.js -t 'cannot pop when empty'    
    
### MyQueue
![queue](../-/jobs/artifacts/master/raw/badges/MyQueue.svg?job=tests)    

Une liste doublement chaînée pour modifier la queue facilement et parcourir dans les deux sens
    
- Le contrat est défini par la même interface que pour `MyFirstQueue` : `Queue` dans **types/queue.ts**
- Le fichier **src/myqueue.ts** contient le squelette de la classe `MyQueue`, ainsi que `DLink`, une structure de [chaînon double](./DoubleLink.md) pour représenter les données.
- Reprendre le code de `MyStack` et mettre à jour avec `DLink` en utilisant la suite de test **tests/stack.js**
- Reprendre le code de `MyList` et mettre à jour avec `DLink` en utilisant la suite de test **tests/list.js**
- Implémenter les méthodes de `Queue` en utilisant la queue de la liste

Lancer les tests :

    // Valider l'implémentation
    yarn test 04_myqueue.test.js

    // Travailler un test en particulier
    yarn test 04_myqueue.test.js -t 'cannot pop when empty'    


### 🚧 MyArrayList
    
## Commandes

Les commandes `yarn` peuvent être remplacée par `npm run`
    
Linter le projet :

    yarn lint
    
Lancer tous les tests du dépôt :

    yarn test

Lancer les tests d'une implémentation:

    yarn test tests/01_mystack.test.js

Lancer un test en particulier

    yarn test tests/01_mystack.test.js -t 'cannot pop when empty'

Générer un rapport de lint

    yarn lint:ci

Générer un rapport de tests

    yarn test:ci

Générer les badges à partir des rapports

    yarn badges:ci
